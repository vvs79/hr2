import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';
import { check } from 'meteor/check';

Meteor.startup(() => {
  Meteor.methods({
    'createPerson': function(personData){
      var name = personData.name, id;
      check(personData.name, String);

      check(personData.phone, String);
      var currentUserId = Meteor.userId();
      personData.createdBy = currentUserId;
      personData.createdAt = new Date();
      personData.updatedAt = new Date();
      id = PersonsList.insert(personData);
     return id;
    }
  });

  Meteor.publish('persons', function(){
    return PersonsList.find();
  });

  Meteor.publish('languages', function(){
    return LanguagesList.find();
  });

  Meteor.publish('educations', function(){
    return EducationsList.find();
  });

  Meteor.publish('experiences', function(){
    return ExperiencesList.find();
  });

  Meteor.publish('communications', function(){
    return CommunicationsList.find();
  });

  Meteor.publish("fileUploads", function () {
    return FilesList.find();
  });

});
