Template.topMenu.helpers({
  'proff': function(){
    return PersonsList.find({}, { sort: {name: 1} });
  },

  'resumeCount': function(){
    return PersonsList.find({}).count();
  },

  'frontEnd': function(){
     var i, 
         persons = PersonsList.find({}).fetch(), counter_fe = 0, counter_be = 0;

     for (i in persons) { if ( (persons[i].proffession).indexOf("Front-end") >= 0 ) {counter_fe += 1;}
                        }
    return counter_fe;
  },

  'backEnd': function(){
    var i, persons = PersonsList.find({}).fetch(), counter_be = 0;
    for (i in persons) { if ( (persons[i].proffession).indexOf("Back-end") >= 0 ) {counter_be += 1;} }
    return counter_be; 
  },

  'new': function(){
    var i, persons = PersonsList.find({}).fetch(), counter = 0;
    for (i in persons) { if ( (persons[i].status).indexOf("New") >= 0 ) {counter += 1;} }
    return counter; 
  },

  'consider': function(){
    var i, persons = PersonsList.find({}).fetch(), counter = 0;
    for (i in persons) { if ( (persons[i].status).indexOf("Consider") >= 0 ) {counter += 1;} }
    return counter; 
  },

  'yes': function(){
    var i, persons = PersonsList.find({}).fetch(), counter = 0;
    for (i in persons) { if ( (persons[i].status).indexOf("Yes") >= 0 ) {counter += 1;} }
    return counter;
  },

  'reserve': function(){
    var i, persons = PersonsList.find({}).fetch(), counter = 0;
    for (i in persons) { if ( (persons[i].status).indexOf("Reserve") >= 0 ) {counter += 1;} }
    return counter; 
  },

  'refused': function(){
    var i, persons = PersonsList.find({}).fetch(), counter = 0;
    for (i in persons) { if ( (persons[i].status).indexOf("Refused") >= 0 ) {counter += 1;} }
    return counter; 
  },

  'no': function(){
    var i, persons = PersonsList.find({}).fetch(), counter = 0;
    for (i in persons) { if ( (persons[i].status).indexOf("No") >= 0 ) {counter += 1;} }
    return counter; 
  }
});
