var sortPers = {name: 1};
var sortPersS = {name: 1};

Template.allResumes.helpers({
  'persons': function(){
    document.getElementById("sort").className = "dropdown inline visible";
    var obj = Object.keys(sortPers), key = obj[0],
        sortPersS = Session.get('sort') || {name: 1},
        objS = Object.keys(sortPersS);

    if (objS[0] == key) { sortPers[key] = 0 - sortPers[key]; }
    else { sortPers = sortPersS;
           obj = Object.keys(sortPers); key = obj[0]; }

    return PersonsList.find({}, { sort: sortPers });
  },

  'communications': function(id_comm){
    var person = PersonsList.findOne({ _id: id_comm }), communication_ids = person.communications_id,
        communication = (communication_ids) ? CommunicationsList.find({ _id: { $in: communication_ids } }) : [] ;
    return communication;
  }
});


Template.topMenu.events({
  'click .sort': function(event){
    event.preventDefault();
    event = event || window.event;
    var target = event.target || event.srcElement,
        key = Object.keys(sortPersS),
        key2 = key[0],
        val = sortPersS[key2],
        text = target.textContent || text.innerText; 

    if (text == "Name") { sortPersS = {name: (0-val)}}
    else if (text == "Age") { sortPersS = {age: (0-val)}}
    else if (text == "Skill") { sortPersS = {skill: (0-val)}}
    else {  };

    Session.set('sort', sortPersS);
  }
});
