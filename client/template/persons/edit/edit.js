var counter_skill = 1;

Template.editResume.rendered = function(){
  document.getElementById("sort").className = "dropdown inline hidden";
  var i, j,
      element = document.querySelector('input[type="hidden"]'),
      id = element.value,
      person = PersonsList.findOne({_id: id}),
      person_work = person.work,
      person_proffession = person.proffession,
      person_status = person.status,
      personStatus = document.getElementsByName("status"),
      personProffession = document.getElementsByName("proffession"),
      personWork = document.getElementsByClassName("personWork");
  for (i in person_work) {
    for (j in personWork) { if (person_work[i] == personWork[j].value) { personWork[j].checked = true;  }   }   
  };
  for (i in person_status) {
    for (j in personStatus) { if (person_status[i] == personStatus[j].value) { personStatus[j].checked = true;    }   }   
  };
  for (i in person_proffession) {
    for (j in personProffession) { if (person_proffession[i] == personProffession[j].value) { personProffession[j].checked = true; }   }   
  };
};

Template.editResume.helpers ({
  'skills': function(){
    element = document.querySelector('input[type="hidden"]');
  }
});

Template.editResume.events({
  'submit .updateForm': function(event){
    event.preventDefault();
    var i, j;
    
    var skills = document.getElementsByName("personSkill"), arr_skills = [];
    for (i in skills) { if (skills[i].value) {arr_skills.push(skills[i].value); } }

    var proffs = document.getElementsByName("personProff"), arr_proffs = [];
    for (i in proffs) { if (proffs[i].value) { arr_proffs.push(proffs[i].value); } }

    var works = document.getElementsByName("personWork"), arr_works = [];
    for (i in works) { if (works[i].checked) { arr_works.push(works[i].value); } }

    var educations_id = [], specialty = document.getElementsByName("education"), from_educ =  document.getElementsByName("educationFrom"),
    to_educ =  document.getElementsByName("educationTo"), place_educ = document.getElementsByName("educationPlace");
    for (i=0;i<specialty.length;++i) {
      educ_id = EducationsList.insert({specialty: specialty[i].value, from: from_educ[i].value, to: to_educ[i].value, place: place_educ[i].value}); 
      educations_id.push(educ_id);
    }

    var experiences_id = [], specialty = document.getElementsByName("job"), from_job =  document.getElementsByName("jobFrom"),
    to_job =  document.getElementsByName("jobTo"), place_job = document.getElementsByName("jobPlace");
    for (i=0;i<specialty.length;++i) {
      job_id = ExperiencesList.insert({specialty: specialty[i].value, from: from_job[i].value, to: to_job[i].value, place: place_job[i].value}); 
      experiences_id.push(job_id);
    }
 
    var languages_id = [], lang = document.getElementsByName("lang"), level =  document.getElementsByName("langLevel");
    for (i=0;i<lang.length;++i) {
      var lang_id = LanguagesList.insert({language: lang[i].value, level: level[i].value}); 
      languages_id.push(lang_id);
    }

    var communications_id = [], date = document.getElementsByName("communDate"), mark =  document.getElementsByName("communValue");
    for (i=0;i<date.length;++i) {
      var comm_id = CommunicationsList.insert({date: date[i].value, mark: mark[i].value}); 
      communications_id.push(comm_id);
    }

    var proffessions = document.getElementsByName("proffession"), arr_proffessions = [];
    for (i in proffessions) { if (proffessions[i].checked) { arr_proffessions.push(proffessions[i].value); } }

    var status = document.getElementsByName("status"), arr_status = [];
    for (i in status) { if (status[i].checked) { arr_status.push(status[i].value); } }


    PersonsList.update({ _id: event.target.personId.value }, 
                        { $set: { name: event.target.personName.value,
                                  skill: arr_skills,
                                  dateOfBirth: event.target.personDate.value,
                                  city: event.target.personCity.value,
                                  phone: event.target.personPhone.value,
                                  age: event.target.personAge.value, 
                                  work: arr_works, 
                                  skype: event.target.personSkype.value,
                                  email: event.target.personEmail.value,
                                  salary: event.target.personSalary.value, 
                                  educations_id: educations_id,
                                  experiences_id: experiences_id,
                                  languages_id: languages_id,
                                  communications_id: communications_id,
                                  proff: arr_proffs,
                                  status: arr_status,
                                  proffession: arr_proffessions,
                                  comment: event.target.comment.value,
                                  updatedAt: new Date()
                        } 
                      });
    Router.go('/resume/' + event.target.personId.value);
  },

  'click .addSkill': function(){
    if (counter_skill < 3)  {
      var newspan = document.createElement('span');
      newspan.innerHTML = '<input type="text" class="form-control" name="personSkill" placeholder="Скіли"/>';
      ++counter_skill;
      document.getElementById('allSkills').appendChild(newspan);
    }
  }
});
