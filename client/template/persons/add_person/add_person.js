var counter_skill, counter_lang, counter_proff, counter_comm, counter_educ, counter_job;

Template.addPersonForm.created = function(){
  counter_skill = counter_lang = counter_proff = counter_comm = counter_educ = counter_job = [1];
};


// Template.addPersonForm.onCreated(function() {
//   console.log(Session.set("skils", 4));
// });

Template.addPersonForm.helpers({
  theFiles: function () {
    document.getElementById("sort").className = "dropdown inline hidden";
    return FilesList.find();
  },
  skills: function() {
    return Session.get("skills") || Session.set("skills", [0]);
  },
  langs: function() {
    return Session.get("langs") || Session.set('langs', [0]);
  },
  comms: function() {
    return Session.get("comms") || Session.set("comms", [0]);
  },
  educs: function() {
    return Session.get("educs") || Session.set('educs', [0]);
  },
  exps: function() {
    return Session.get("exps") || Session.set("exps", [0]);
  },
  profs: function() {
    return Session.get("profs") || Session.set('profs', [0]);
  }

  // counter: function () {
  //   var arr = Session.get('count_sk') || [counter_skill, counter_lang, counter_proff, counter_comm, counter_educ, counter_job];
  //   var count_skill = arr[0] || [1]; console.log('count_skill - ' + count_skill);
  //   var count_lang = arr[1] || [1]; console.log('count_lang - ' + count_lang);
  //   var count_proff = [1];
  //   var count_comm = arr[3] || [1]; console.log('count_comm - ' + count_comm);
  //   var count_educ = [1];
  //   var count_job = [1];
  //   return { count_skill: count_skill,
  //            count_lang: count_lang,
  //            count_proff: count_proff,
  //            count_comm: count_comm,
  //            count_educ: count_educ,
  //            count_job: count_job
  //          };
  // }
});

Template.addPersonForm.events({
  'submit .insertForm': function(event){
    event.preventDefault();
    var personData = {};
    var files_id = "";
    var files_id_arr = "";
    var i = 0;

    personData.name = event.target.personName.value;

    var skills = document.getElementsByName("personSkill"), arr_skills = [];
    for (i in skills) { if (skills[i].value) { arr_skills.push(skills[i].value); } }
    for (i in skills) { skills[i].value = ""; }
    for (i=0; i<=(counter_skill-2); i++) { skills[i].parentNode.removeChild(skills[i]); }
    personData.skill = arr_skills;

    var proffs = document.getElementsByName("personProff"), arr_proffs = [];
    for (i in proffs) { if (proffs[i].value) { arr_proffs.push(proffs[i].value); } }
    for (i in proffs) { proffs[i].value = ""; }
    for (i=0; i<=(counter_proff-2); i++) { proffs[i].parentNode.removeChild(proffs[i]); }
    personData.proff = arr_proffs;

    personData.dateOfBirth = event.target.personDate.value;
    personData.city = event.target.personCity.value;
    personData.phone = event.target.personPhone.value;
    personData.skype = event.target.personSkype.value;
    personData.email = event.target.personEmail.value;
    personData.salary = event.target.personSalary.value;
    personData.age = event.target.personAge.value;
    personData.comment = event.target.comment.value;

    var works = document.getElementsByName("personWork"), arr_works = [];
    for (i in works) { if (works[i].checked) { arr_works.push(works[i].value);} }
    for (i in works) { works[i].checked = false; }
    personData.work = arr_works;


    var educations_id = [];
    var specialty = document.getElementsByName("education"), from_educ =  document.getElementsByName("educationFrom"),
    to_educ =  document.getElementsByName("educationTo"), place_educ = document.getElementsByName("educationPlace");
    for (i=0;i<specialty.length;++i) { if (specialty[i].value) {
      educ_id = EducationsList.insert({specialty: specialty[i].value, from: from_educ[i].value, to: to_educ[i].value, place: place_educ[i].value}); 
      educations_id.push(educ_id); }
    }
    personData.educations_id = educations_id;
    for (i in specialty) { specialty[i].value = from_educ[i].value = to_educ[i].value = place_educ[i].value = ""; }
    if (counter_educ > 1) {
      var educ_div = document.getElementsByClassName("educDiv");
      for (i=0;i<=(counter_educ-2);++i) { educ_div[0].parentNode.removeChild(educ_div[0]); }
    }


    var experiences_id = [];
    var specialty = document.getElementsByName("job"), from_job =  document.getElementsByName("jobFrom"),
    to_job =  document.getElementsByName("jobTo"), place_job = document.getElementsByName("jobPlace");
    for (i=0;i<specialty.length;++i) { if (specialty[i].value) {
      job_id = ExperiencesList.insert({specialty: specialty[i].value, from: from_job[i].value, to: to_job[i].value, place: place_job[i].value}); 
      experiences_id.push(job_id); }
    }
    personData.experiences_id = experiences_id;
    for (i in specialty) { specialty[i].value = from_job[i].value = to_job[i].value = place_job[i].value = ""; }
    if (counter_job > 1) {
      var job_div = document.getElementsByClassName("jobDiv");
      for (i=0;i<=(counter_job-2);++i) { job_div[0].parentNode.removeChild(job_div[0]);}
    }

    var languages_id = [];
    var lang = document.getElementsByName("lang"), level =  document.getElementsByName("langLevel");
    for (i=0;i<lang.length;++i) { if (lang[i].value != 'Мова') {
      var lang_id = LanguagesList.insert({language: lang[i].value, level: level[i].value}); 
      languages_id.push(lang_id); }
    }
    personData.languages_id = languages_id;
    for (i in lang) { lang[i].value = level[i].value = ""; }
    if (counter_lang > 1) {
      var lang_div = document.getElementsByClassName("langDiv");
      for (i=0;i<=(counter_lang-2);++i) { lang_div[0].parentNode.removeChild(lang_div[0]);}
    }

    var communications_id = [];
    var date = document.getElementsByName("communDate"), mark =  document.getElementsByName("communValue");
    for (i=0;i<date.length;++i) { if (mark[i].value) {
      var comm_id = CommunicationsList.insert({date: date[i].value, mark: mark[i].value}); 
      communications_id.push(comm_id); }
    }
    personData.communications_id = communications_id;
    for (i in date) { date[i].value = mark[i].value = ""; }
    if (counter_comm > 1) {
      var comm_div = document.getElementsByClassName("commDiv");
      for (i=0;i<=(counter_comm-2);++i) { comm_div[0].parentNode.removeChild(comm_div[0]);}
    }
    

    files_id_arr = document.getElementsByClassName("files_id");
    for (i in files_id_arr) {
      if (typeof files_id_arr[i].innerHTML !== "undefined") {
        files_id += ( files_id_arr[i].innerHTML + "," );
      }
    }
    personData.files_id = files_id;

    var proffessions = document.getElementsByName("proffession"), arr_proffessions = [];
    for (i in proffessions) { if (proffessions[i].checked) { arr_proffessions.push(proffessions[i].value); } }
    personData.proffession = arr_proffessions;

    var status = document.getElementsByName("status"), arr_status = [];
    for (i in status) { if (status[i].checked) { arr_status.push(status[i].value); } }
    personData.status = arr_status;

    event.target.personName.value = event.target.personDate.value = event.target.personCity.value = event.target.personPhone.value = "";
    event.target.personSkype.value = event.target.comment.value = event.target.personEmail.value = event.target.personSalary.value = "";
    event.target.personAge.value = "";

    var id;
    Meteor.call('createPerson', personData, function (err, res) {
      if (err) { Router.go('/error'); }
      else { Router.go('/resume/' + res); }
    });
  },


  'click .addSkill': function(){
      var arr = Session.get('skills');
      if(typeof(arr) != 'undefined') {
        if (Session.get('skills').length < 3) arr.push(1);
      } else {
        arr = [0];
      }
      Session.set("skills", arr);
  },

  'click .addLang': function(){
      var arr = Session.get('langs');
      if(typeof(arr) != 'undefined') {
        if (Session.get('langs').length < 3) arr.push(1);
      } else {
        arr = [0];
      }
      Session.set("langs", arr);
  },

  'click .addProff': function(){
      var arr = Session.get('profs');
      if(typeof(arr) != 'undefined') {
        if (Session.get('profs').length < 3) arr.push(1);
      } else {
        arr = [0];
      }
      Session.set("profs", arr);
  },

  'click .addCommun': function(){
      var arr = Session.get('comms');
      if(typeof(arr) != 'undefined') {
        if (Session.get('comms').length < 3) arr.push(1);
      } else {
        arr = [0];
      }
      Session.set("comms", arr);
  },

  'click .addEduc': function(){
      var arr = Session.get('educs');
      if(typeof(arr) != 'undefined') {
        if (Session.get('educs').length < 3) arr.push(1);
      } else {
        arr = [0];
      }
      Session.set("educs", arr);
  },

  'click .addJob': function(){
      var arr = Session.get('exps');
      if(typeof(arr) != 'undefined') {
        if (Session.get('exps').length < 3) arr.push(1);
      } else {
        arr = [0];
      }
      Session.set("exps", arr);
  },


  'click #deleteFileButton ': function (event) {
    FilesList.remove({_id: this._id});
  },

  'change .upload': function (event, template) {
    FS.Utility.eachFile(event, function (file) {
      var myFile = new FS.File(file);
      FilesList.insert(myFile, function (err, fileObj) {
        if (!err) {
        }
        else {
        }
      });
    });
  }

});
