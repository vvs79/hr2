Template.removePerson.helpers({
  'person': function(){
    document.getElementById("sort").className = "dropdown inline hidden";
    return PersonsList.find({}, { sort: {name: 1} });
  },

  'selectedClass': function(){
    var personId = this._id;
    var selectedPerson = Session.get('selectedPerson');
    if(personId == selectedPerson) {return "selected";}
  },

  'selectedPerson': function(){
    var selectedPerson = Session.get('selectedPerson');
    return PersonsList.findOne({ _id: selectedPerson });
  }
});

Template.removePerson.events({
  'click .person': function(){
    var personId = this._id;
    Session.set('selectedPerson', personId);
  },

  'click .remove': function(){
    var selectedPerson = Session.get('selectedPerson');
    PersonsList.remove({ _id: selectedPerson });
  }
});
