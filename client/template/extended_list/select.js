Template.select.helpers({
  'selects': function(){
    var i, arr = Session.get('select') || [], proff = arr[0], status = arr[1], arr_pers = [],
        persons = PersonsList.find({}).fetch(), counter = 0;

    for (i=0;i<persons.length;i++) {
      if ( (persons[i].proffession).indexOf(proff) >= 0 && (persons[i].status).indexOf(status) >= 0 ) { counter += 1; }
      else { persons.splice(i, 1); --i; }
    }
    return persons;
  }
});


Template.topMenu.events({
   'click #but': function(event){
     event.preventDefault();
     var proff = document.getElementById("proff_id").value,
         status = document.getElementById("status").value, arr=[];
     arr.push(proff);
     arr.push(status);
     Session.set('select', arr);
     Router.go('/select');
   }
});
