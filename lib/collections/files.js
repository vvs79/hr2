FilesList = new FS.Collection("files", {
  stores: [new FS.Store.FileSystem("files", {path: "~/uploads"})]
});

FilesList.allow({
  insert: function (userId, doc) {
    return true;
  },
  update: function (userId, doc) {
    return true;
  },
  remove: function (userId, doc) {
    return true;
  },
  download: function (userId, doc) {
    return true;
  }
});
