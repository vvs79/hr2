﻿PersonsList = new Mongo.Collection('persons'),
  PersonsIndex = new EasySearch.Index({
    collection: PersonsList,
    fields: ['name', 'skill', 'age', 'city', 'phone', 'salary', 'email', 'skype'],
    engine: new EasySearch.Minimongo()
  });

PersonsListSchema = new SimpleSchema({
  name: { type: String, label: "Wrong name", min: 3, max: 100 },
  phone: { type: String, label: "Wrong phone number", max: 12, optional: true , regEx: /[0-9]/ },
  skype: { type: String, label: "Wrong skype", max: 50, optional: true },
  email: { type: String, label: "Wrong email", regEx: SimpleSchema.RegEx.Email, optional: true },
  skill: { type: [String], label: "Wrong skills", max: 150, optional: true },
  dateOfBirth: { type: Date, label: "Wrong dateOfBirth", optional: true },
  age: { type: SimpleSchema.Integer, label: "Wrong age", max: 3, min: 0, optional: true },
  city: { type: String, label: "Wrong city", max: 30, optional: true },
  salary: { type: SimpleSchema.Integer, label: "Wrong salary", max: 7, optional: true },
  work: { type: [String], label: "Wrong works", max: 100, optional: true },
  educations_id: { type: [String], label: "Wrong educations_id", max: 100, optional: true },
  experiences_id: { type: [String], label: "Wrong experiences_id", max: 100, optional: true },
  languages_id: { type: [String], label: "Wrong languages_id", max: 100, optional: true },
  communications_id: { type: [String], label: "Wrong communications_id", max: 100, optional: true },
  proff: { type: [String], label: "Wrong proffs", max: 200, optional: true },
  proffession: {type: [String], label: "Wrong proffession", max: 50, optional: true},
  status: {type: [String], label: "Wrong status", max: 100, optional: true},
  comment: { type: String, label: "Wrong comment", max: 1000, optional: true }
});
PersonsList.attachSchema(PersonsListSchema);

if ( Meteor.isServer ) {
  PersonsList._ensureIndex( { name: 1, skill: 1, age: 1 } );
}

PersonsList.allow({
  insert: () => true,
  update: () => true,
  remove: () => true
});
