selectPerson = function(personId){
  var personFiles = [], currentResume = personId, pers = PersonsList.findOne({ _id: currentResume }),
      education_ids = pers.educations_id, experience_ids = pers.experiences_id, language_ids = pers.languages_id,
      communication_ids = pers.communications_id, i, files, experiences = education = languages = communication = {},
      skills = pers.skill,
      proffs = pers.proff,
      ar_skills,
      arr_skills = [],
      ar_proffs,
      arr_proffs = [];
  if (skills) { ar_skills = (Array.isArray(skills)) ? skills : skills.split(","); 
      for (i in ar_skills) { arr_skills.push({skill: ar_skills[i]}); }
  }
  if (proffs) { ar_proffs = (Array.isArray(proffs)) ? proffs : proffs.split(",");
      for (i in proffs) { arr_proffs.push({proff: proffs[i]}); }
  }
  experiences = (experience_ids) ? ExperiencesList.find({ _id: { $in: experience_ids } }) : [];
  education = (education_ids) ? EducationsList.find({ _id: { $in: education_ids } }) : [];
  languages = (language_ids) ? LanguagesList.find({ _id: { $in: language_ids } }) : [];
  communication = (communication_ids) ? CommunicationsList.find({ _id: { $in: communication_ids } }) : [];
  if (typeof PersonsList.findOne({ _id: currentResume }).files_id !== "undefined") {
      files = (PersonsList.findOne({ _id: currentResume }).files_id).split(",");
  }
  for (i in files) { personFiles.push(FilesList.findOne({ _id: files[i] })); }
  personFiles.pop();
  return {person: PersonsList.findOne({ _id: currentResume }), personFiles, experiences, education, languages, communication, arr_skills, arr_proffs};
};

Router.route(('/'), { name: 'home', template: 'allResumes' });
Router.route('/addPersonForm');
Router.route('/removePerson');
Router.route('/select');
Router.route('/allResumes');

Router.route('/error');
Router.route('/search');

Router.configure({
  layoutTemplate: 'main'
});

Router.route('/resume/:_id', { template: 'showResume',
  data: function(){
    return selectPerson(this.params._id);
  }
});

Router.route('/edit/:_id', { template: 'editResume',
    data: function(){
      return selectPerson(this.params._id);
    }
});
